﻿using bookStore_WEB.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bookStore_WEB.Controllers.API
{
    public class BooksAPIController : ApiController
    {
        public IHttpActionResult Get()
        {
            IList<Books> books = null;

            using (var ctx = new BOOKSTORE_DB())
            {
                books = ctx.Books.ToList();
            }

            if (books.Count == 0)
            {
                return NotFound();
            }

            return Ok(books);
        }

        public IHttpActionResult Get(int id)
        {
            Books book = null;

            using (var ctx = new BOOKSTORE_DB())
            {
                book = ctx.Books.FirstOrDefault(b => b.BookId == id);
            }

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }
    }
}
