﻿using bookStore_WEB.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace bookStore_WEB.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            IEnumerable<Books> books = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2183/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BooksAPI");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Books>>();
                    readTask.Wait();

                    books = readTask.Result;
                    Session["booksForSimilar"] = books;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    books = Enumerable.Empty<Books>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(books);
        }

        public ActionResult ProductDetail(int pId)
        {
            Books book = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2183/api/");
                //HTTP GET
                var responseTask = client.GetAsync("BooksAPI?id=" + pId.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Books>();
                    readTask.Wait();

                    book = readTask.Result;
                }
            }

            ViewBag.SimilarProducts = (IEnumerable<Books>)Session["booksForSimilar"];
            return View(book);
        }
    }
}