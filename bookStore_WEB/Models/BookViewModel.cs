﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web;
using bookStore_WEB.DAL;

namespace bookStore_WEB.Models
{
    public class BookViewModel
    {
        public IEnumerable<Books> SimilarProducts { get; set; }
        public Books SingleProduct { get; set; }
    }
}